DESCRIPTION ?= "Qt is a cross-platform application development framework for desktop, embedded and mobile."
HOMEPAGE ?= "https://www.qt.io"

QT_MODULE ?= "${BPN}"
QT_MODULE_BRANCH ?= "dev"
QT_MODULE_BRANCH_PARAM ?= "branch=${QT_MODULE_BRANCH};nobranch=1"
QT_MODULE_REPO ?= "${QT_MODULE}.git"

SRC_URI = "${QT_GIT}/${QT_GIT_PROJECT}/${QT_MODULE_REPO};name=${QT_MODULE};${QT_MODULE_BRANCH_PARAM};protocol=${QT_GIT_PROTOCOL}"

CVE_PRODUCT = "qt"

S = "${WORKDIR}/git"

PV = "6.5.0"

SRCREV = "${SRCREV_${QT_MODULE}}"

SRCREV_tqtc-qmlcompilerplus = "4735412189bf05eb9c1d6698a04910e1a2f9860c"
SRCREV_tqtc-qtvncserver = "aea6e5262ee6df49b23296b47ff91046f345dcad"
SRCREV_qt3d = "0bfdee7e20af68b38072c042161220472515a281"
SRCREV_qt3d-assimp = "5a38cd0a03015ceabbd5bc6efb0730dde1ef74e5"
SRCREV_qt5compat = "d9b6b75f7e0252a6b1ddf38ce7ca7691863bdfa9"
SRCREV_qtbase = "6a027a8677888bcc881929f47185603778e4c50e"
SRCREV_qtapplicationmanager = "68888133e2aa7dd3ed2ef1c4d1095065749d7e7f"
SRCREV_qtcharts = "eb66f114b496f7b783fd22267c57bad869790ced"
SRCREV_qtcoap = "5584c08c5d19458b08a9e609c59bf46fe6ea619d"
SRCREV_qtconnectivity = "e53674463e3e8d419a49855d3fc5bcf9492f5f26"
SRCREV_qtdatavis3d = "4f18fddff595cc5b49fd5a665ccdfc9b80174dfc"
SRCREV_qtdeclarative = "5595809245cfedeb86657277b8354f45ddda2528"
SRCREV_qtdeviceutilities = "99adb8059cf4fae940f4e2654c8ecb16f92180b9"
SRCREV_qtgrpc = "cd33c3cd52a1b2151d705e0c394dace796d0b45a"
SRCREV_qthttpserver = "7c0327f8b09f93117c0edd55f05ccf059b8395b8"
SRCREV_qtimageformats = "16bae1a4ecf8953489493471ab8546faabe57d44"
SRCREV_qtinterfaceframework = "bebbd231e1e6f86c999efcb0bfb02bc98e422617"
SRCREV_qtlanguageserver = "7b43782262af2c067ca4aecfbb6701b05fd64d46"
SRCREV_qtlocation = "8f6bd3b108826e3189d1b7f3280e748a473b2a1b"
SRCREV_qtlottie = "349e99f86e4d2e3d34ef44d300020e49ca879004"
SRCREV_qtmqtt = "5581e41a066e2d2352115e7e8938c42cae47a15e"
SRCREV_qtmultimedia = "413f2cfe8a6923b1b0d5935a3ce3dbd3ab4f9ff7"
SRCREV_qtnetworkauth = "2c2a4db9d084b1926b3983d4306d83533f283b3c"
SRCREV_qtopcua = "b97040a302eee0bfe258143f04f2ef19d1ab47be"
SRCREV_qtpositioning = "87d359b31df72349700caf1321b9fd2232bff50a"
SRCREV_qtquick3d = "d607426ecad513b8d3a1210d06ea9994b0b1bc6a"
SRCREV_qtquick3d-assimp = "fd7cd819e9e130385e17e707da31e60b82e787d0"
SRCREV_qtquick3dphysics = "07b9368aac1ae9fd80dc00766f387507839d05c7"
SRCREV_qtquickdesigner-components = "eef765c5ed34eac8c1294fbeda55d273f916ca86"
SRCREV_qtquicktimeline = "bf3df3ae29921a16e4549f9af7e040d4d1fc166f"
SRCREV_qtremoteobjects = "3bd1d8725c3af77c70c7deb3601584240bcb0291"
SRCREV_qtscxml = "3335de9877f113034bab43de34d8ac3af5579db0"
SRCREV_qtsensors = "1dd250d313446dea1b4b63bb59a0b960a2e37911"
SRCREV_qtserialbus = "b47bf24b53e4d9ca71c045f6de7a3019c2c4deb4"
SRCREV_qtserialport = "1bdde6cc5ce82a795b9659e0e93c8b4ecb5d0a4b"
SRCREV_qtshadertools = "3d53b994ca7e3be407c93424f745e49f1d5ff029"
SRCREV_qtspeech = "43a55fd783a8cbcdd1924a0d8e7e78fce310eff3"
SRCREV_qtsvg = "4df3e31a72c9a3524870c8953b9ae6e9192dcc0d"
SRCREV_qttools = "dc5fd0c416148e1de89c486e48c3b3f7e616ad17"
SRCREV_qttools-qlitehtml = "bd70f93ce41443a6a90a269531393f575685283e"
SRCREV_qttools-qlitehtml-litehtml = "e3f37e3d5a6931ddecf81a9d746fc3e9a3475998"
SRCREV_qttranslations = "3ae47db095993ab04d1afe64e0e3594862fab600"
SRCREV_qtvirtualkeyboard = "18d65001699a68e35dbaeb44781f13756014061c"
SRCREV_qtwayland = "085f5b07a104c46f489969cea0a8a343bb8569e0"
SRCREV_qtwebchannel = "16a69278e5f92d10947f70d138c0059abdba7ed5"
SRCREV_qtwebengine = "0fa4473fe42139a39df64a09565733f198b2ce39"
SRCREV_qtwebengine-chromium = "be36115f03e8e70abe88c890ad6642e1b909da1d"
SRCREV_qtwebsockets = "e58390e589768f467d99c0f8a3e4e9d3722e5edb"
SRCREV_qtwebview = "d87daf1a6575d549896895b75e55829fa55d16c0"
